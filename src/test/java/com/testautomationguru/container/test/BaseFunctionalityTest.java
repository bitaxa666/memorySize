package test.java.com.testautomationguru.container.test;

import io.github.cdimascio.dotenv.Dotenv;
import org.testng.annotations.Test;
import test.java.com.testautomationguru.container.test.memory.HeapSize;
import test.java.com.testautomationguru.container.test.page.Dashboard.DashboardPage;
import test.java.com.testautomationguru.container.test.utils.logging.CustomReporter;

/**
 * Created by user on 7/17/18.
 */
public class BaseFunctionalityTest extends BaseTestLocal {

    private DashboardPage dashboardPage;
    private HeapSize heapSize;
    private Dotenv dotenv;

    //Successfully logged in
    @Test(priority = 0)
    public void loggedIn(){

        dashboardPage = new DashboardPage(driver);
        heapSize = new HeapSize(driver);
        dotenv = Dotenv.configure()
                .directory("./ENV/")
                .ignoreIfMalformed()
                .ignoreIfMissing()
                .load();
        driver.get(dotenv.get("WEB_ADDRESS"));
        CustomReporter.logAction("Browser launched and navigated to the Login page");
        dashboardPage.loginUser(dotenv.get("USER_NAME"), dotenv.get("USER_PWD"));
        heapSize.getBeforeUsedJSHeapSize();
/*
        try {
            heapSize.generateAnalyzeDocument(heapSize.analyzeHeapSnapshot(heapSize.takeHeapSnapshot()));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Analyze file is not created");
        }
*/
    }

    @Test(priority = 10, dependsOnMethods = {"loggedIn"})
    public void createSchema() {

        dashboardPage.createNewDashboardPage();
        dashboardPage.createNewCharts();
        dashboardPage.addSuggestionsToCharts();
        dashboardPage.loopOfSaving();
        heapSize.getAfterUsedJSHeapSize();

        //Loop for edit function
        for(int i = 0; i<5; i++)
        {
            dashboardPage.loopOfEdit();
            heapSize.getAfterUsedJSHeapSize();
        }
        //Close Dashboard and generate output file
        dashboardPage.closeDashboardItem();
        heapSize.getAfterUsedJSHeapSize();
        heapSize.analysisMemoryLeaks();
        heapSize.generateOutputMemory(dotenv.get("JSON_SIZE_FILE_NAME"));

    /*
        try {
            heapSize.generateAnalyzeDocument(heapSize.analyzeHeapSnapshot(heapSize.takeHeapSnapshot()));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Analyze file is not created");
        }*/

    }
}
