package test.java.com.testautomationguru.container.test.chart;

import be.ceau.chart.Chart;

import java.io.IOException;


/**
 * Created by user on 8/6/18.
 */
public abstract class ChartBasic {

    public abstract Chart createChart();

    public void chartTest() throws IOException {
        Opener.inBrowser(createChart());
    }


}
