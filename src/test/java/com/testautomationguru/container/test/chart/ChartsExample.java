package test.java.com.testautomationguru.container.test.chart;

import be.ceau.chart.Chart;
import be.ceau.chart.LineChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.LineData;
import be.ceau.chart.dataset.LineDataset;
import be.ceau.chart.enums.BorderCapStyle;
import be.ceau.chart.enums.BorderJoinStyle;
import be.ceau.chart.options.LineOptions;
import io.github.cdimascio.dotenv.Dotenv;
import org.testng.annotations.Test;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


/**
 * Created by user on 8/6/18.
 * Article with charts example - https://github.com/mdewilde/chart
 */
public class ChartsExample extends ChartBasic{

    private JsonArray timeData;
    private JsonArray memorySizeList;
    private Map chartData;
    private Dotenv dotenv;

    public ChartsExample() {
        this.timeData = null;
        this.memorySizeList = null;
        this.chartData = null;
        dotenv = Dotenv.configure()
                .directory("./ENV/")
                .ignoreIfMalformed()
                .ignoreIfMissing()
                .load();
    }

    @Override
    public Chart createChart() {
        LineChart chart = new LineChart();
        chart.setData(TestFactory.randomInstance(LineData.class));
        chart.setOptions(TestFactory.randomInstance(LineOptions.class));
        System.out.println("Chart is" + (chart.isDrawable() ? " " : " NOT ") + "drawable");
        return chart;
    }

    @Test
    public void createExampleChart() throws IOException {

        LineChart lineChart = new LineChart();
        try {
        jsonOutput(System.getProperty("user.dir") + "/target/" + dotenv.get("JSON_SIZE_FILE_NAME"));

        String[] timeArray = new String[timeData.size()];
        if(timeData.size() != 0) {
            for (int i = 0; i < timeArray.length; i++) {
                timeArray[i] = String.valueOf(timeData.get(i));
            }
        } else {
            for (int i = 0; i < 5; i++) {
                timeArray[i] = String.valueOf(i);
            }
        }

        String[] memoryArray = new String[memorySizeList.size()];
        if(memorySizeList.size() != 0) {
            for (int i = 0; i < memoryArray.length; i++) {
                memoryArray[i] = String.valueOf(memorySizeList.get(i));
            }
        } else {
            for (int i = 10; i < 15; i++) {
                timeArray[i] = String.valueOf(i);
            }
        }

        int[] myTestSizeArray = new int[memoryArray.length];

            myTestSizeArray = StringArrToArray(memoryArray);

            lineChart.setData(createLineData(timeArray, myTestSizeArray));

        Opener.inBrowser(lineChart);
        Opener.createHtmlDoc(lineChart, dotenv.get("HTML_REPORT_FILE"));
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println("We have some problem with data for charts, or with .json");
        }
    }

    private int[] StringArrToArray(String[] s) throws NumberFormatException{
        int[] result = new int[s.length];
        for (int i = 0; i < s.length; i++) {
            result[i] = new Integer(s[i]).intValue();
        }
        return result;
    }

    private LineData createLineData(String[] timeArray, int[] dataValue) {

        return new LineData()
                .addDataset(createLineDataset(dataValue))
                .addLabels(timeArray);

    }

    private LineDataset createLineDataset(int[] dataValue) {

        /**
         * @ function for chart drawing
         * Chart creates and draws in browser windows
         */
        return new LineDataset()
                .setLabel("JS Memory Heap Size")
                .setFill(false)
                .setLineTension(0.1f)
                .setBackgroundColor(new Color(75, 192, 192, 0.4))
                .setBorderColor(new Color(75,192,192,1))
                .setBorderCapStyle(BorderCapStyle.BUTT)
                .setBorderDashOffset(0.0f)
                .setBorderJoinStyle(BorderJoinStyle.MITER)
                .addPointBorderColor(new Color(75, 192, 192, 1))
                .addPointBackgroundColor(new Color(255, 255, 255, 1))
                .addPointBorderWidth(2)
                .addPointHoverRadius(5)
                .addPointHoverBackgroundColor(new Color(75,192,192,1))
                .addPointHoverBorderColor(new Color(220,220,220,1))
                .addPointHoverBorderWidth(8)
                .addPointRadius(2)
                .addPointHitRadius(10)
                .setSpanGaps(false)
                .setData(dataValue);
    }

    @SuppressWarnings("unchecked")
    private Map jsonOutput (String fileName) throws IOException {

        chartData = new HashMap();
        File jsonInputFile = new File(fileName);
        InputStream is=null;

        /**
         * @ function into try block
         * Create JsonReader from Json., Get the JsonObject structure from JsonReader., read string data
         */
        try {
            is = new FileInputStream(jsonInputFile);
            JsonReader reader = Json.createReader(is);
            JsonObject empObj = reader.readObject();
            reader.close();
            System.out.println("resultMemoryValue: " + empObj.getJsonNumber("resultMemoryValue"));
            timeData = empObj.getJsonArray("time");
            memorySizeList = empObj.getJsonArray("usedJSHeapSize");
            for (int i=0; i<timeData.size(); i++) {
                chartData.put(timeData.get(i), memorySizeList.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("JSON file does not found");
        } finally {
            is.close();
        }
        return chartData;
    }
}
