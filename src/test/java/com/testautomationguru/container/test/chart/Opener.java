package test.java.com.testautomationguru.container.test.chart;

import be.ceau.chart.Chart;

import java.awt.*;
import java.io.*;

/**
 * Created by user on 8/6/18.
 */
public class Opener {

    /**
     * Serialize the given chart, create a HTML page for it, open the HTML file
     * in the default browser
     *
     * @param chart
     *            a {@link Chart} implementation, not {@code null}
     * @throws IOException
     */
    public static void inBrowser(Chart chart) throws IOException {

        if (!chart.isDrawable()) {
            throw new IllegalArgumentException("chart is not drawable");
        }

        File tmp = File.createTempFile("chart_test_", ".html");

        PrintWriter out = new PrintWriter(tmp);
        out.write(createWebPage(chart.getType(), chart.toJson()));
        out.close();

        Desktop.getDesktop().browse(tmp.toURI());
    }

    public static void createHtmlDoc (Chart chart, String fileName) {
        String fileHtmlSavedPath = System.getProperty("user.dir") + "/target/";

        BufferedWriter htmlnWriter = null;
        File htmlChart = new File(fileHtmlSavedPath + fileName);

        try {
        //when file file doesn't exists, then create it
            if(!htmlChart.exists()) {
                htmlnWriter = new BufferedWriter(new OutputStreamWriter(
                        //new FileOutputStream(fileSavedPath + fileNameJson, true), "utf-8"));
                        new FileOutputStream(htmlChart), "utf-8"));
                String htmlCode = createWebPage(chart.getType(), chart.toJson());
                htmlnWriter.write(htmlCode);
                htmlnWriter.flush();
            } else {
                htmlChart.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (htmlnWriter != null)
                    htmlnWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static String createWebPage(String type, String json) {
        String line = System.getProperty("line.separator");

        return new StringBuilder()
                .append("<!DOCTYPE html>")
                .append(line)
                .append("<html lang='en'>")
                .append(line)
                .append("<head>")
                .append(line)
                .append("<meta charset='UTF-8'>")
                .append(line)
                .append("<title>Chart.java test page - ").append(type).append("</title>")
                .append(line)
                .append("<meta name='author' content='Mihayluk Vitaliy'>")
                .append(line)
                .append("<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js'></script>")
                .append(line)
                .append("<script>")
                .append("function r(e,t){new Chart(document.getElementById(e).getContext('2d'),t)}")
                .append("</script>")
                .append(line)
                .append("</head>")
                .append(line)
                .append("<body>")
                .append(line)
                .append("<canvas id='c' style='border:1px solid #555;'></canvas>")
                .append(line)
                .append("<div><pre>").append(json).append("</pre></div>")
                .append(line)
                .append("<script>")
                .append(line)
                .append("r('c', ").append(json).append(");")
                .append("</script>")
                .append(line)
                .append("</body>")
                .append(line)
                .append("</html>")
                .toString();
    }

}
