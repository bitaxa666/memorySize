package test.java.com.testautomationguru.container.test.memory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import test.java.com.testautomationguru.container.test.utils.logging.CustomReporter;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 7/18/18.
 */
public class HeapSize  {

    private WebDriver driver;
    private String fileSavedPath;
    private JSONObject jsonSize;

    private JSONObject jsonMaxSize;
    private JSONArray timeList;
    private JSONArray memoryList;
    private JSONArray memoryMaxList;


    public HeapSize(WebDriver driver) {
        this.driver = driver;
        this.fileSavedPath = System.getProperty("user.dir") + "/target/";
        this.jsonSize = new JSONObject();
        this.timeList = new JSONArray();
        this.memoryList = new JSONArray();

        this.memoryMaxList = new JSONArray();
    }

    //First test after login user - all charts is empty
    public void getBeforeUsedJSHeapSize() {
        driver.navigate().refresh();
        getJSHeapSize();
    }
    //Test when user created charts - have some data
    public void getAfterUsedJSHeapSize() {
        getJSHeapSize();
    }

    //getJSHeapSize method
    private void getJSHeapSize() {
        Long memoryHeapSize =(Long) ((JavascriptExecutor)driver).executeScript("return window.performance.memory.usedJSHeapSize");

        System.out.println(String.format("[%-12s]", LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME)) + ":" + Long.toString(memoryHeapSize));
        timeList.put(String.format("[%-12s]", LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME)));
        memoryList.put(memoryHeapSize);
    }

    //JSHeapSize analysis
    public Double analysisMemoryLeaks() {
        Long firstItem = (Long) memoryList.get(0);
        int sizeOfElements = memoryList.length() - 1;
        Long endItem = (Long) memoryList.get(sizeOfElements);

        double testResultSize = ((double) endItem /(double)firstItem) * 100;
        jsonSize.put("resultMemoryValue", testResultSize);
        System.out.println(testResultSize + " %");
        if(testResultSize > 125) {
            System.out.println("Test result has a bad value (more than 25%)");
        } else if(testResultSize > 100 & testResultSize <= 125) {
            System.out.println("Test result has a good value (less than 25%)");
        } else if(testResultSize < 100) {
            System.out.println("Test result in standard range. GC works using normal mode.");
        }
        else System.out.println("Something went wrong");

        return testResultSize;
    }

    public void generateOutputMemory(String fileNameJson) {

        BufferedWriter jsonWriter = null;
        File file = null;
        try {

            //Values added to the JSON file
            jsonSize.put("time", timeList);
            jsonSize.put("usedJSHeapSize", memoryList);
           // file = new File(fileSavedPath + generateUniqueFileName() + fileNameJson);
            file = new File(fileSavedPath + fileNameJson);

            //when file file doesn't exists, then create it
            if(!file.exists()) {
                file.createNewFile();
            }

            //data writes into file
            jsonWriter = new BufferedWriter(new OutputStreamWriter(
                    //new FileOutputStream(fileSavedPath + fileNameJson, true), "utf-8"));
                    new FileOutputStream(file), "utf-8"));
            jsonWriter.write(jsonSize.toString());
            jsonWriter.flush();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File is not created.");
        } finally {
            int timeListLength = timeList.length();
            for (int i = timeListLength - 1; i >= 0; i--) {
                timeList.remove(i);
            }

            int memoryListLength = memoryList.length();
            for (int i = memoryListLength - 1; i >= 0; i--) {
                memoryList.remove(i);
            }

            try {
                if (jsonWriter != null)
                    jsonWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //File name generates in this method
    private String generateUniqueFileName() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss") ;
        return new String(dateFormat.format(date));
    }

    // JS heap snapshot
    /**
     * See https://code.google.com/p/chromedriver/issues/detail?id=519<br/>
     * Note: slow, each call takes a couple of seconds
     *
     * @return JS heap snapshot
     */
    @SuppressWarnings("unchecked")
    public Map<String, ?> takeHeapSnapshot() {
        long startTime = System.currentTimeMillis();
        Map<String, ?> snapshot = (Map<String, ?>) ((JavascriptExecutor) driver).executeScript(":takeHeapSnapshot");
        CustomReporter.log("took heap snapshot in " + (System.currentTimeMillis() - startTime) + " ms");
        return snapshot;
    }
    /**
     * Analyzes the data in the snapshot and returns summary data
     */
    @SuppressWarnings("unchecked")
    public JSONObject analyzeHeapSnapshot(Map<String, ?> data) throws IOException {
        Map<String, ?> metadata = (Map<String, ?>) data.get("snapshot");

        int nodeCount = ((Number) metadata.get("node_count")).intValue();
        // "node_fields": ["type","name","id","self_size","edge_count"]
        List<Number> nodes = (List<Number>) data.get("nodes");

        int totalSize = 0;
        for (int i = 0; i < nodeCount; i++) {
            totalSize += nodes.get(5 * i + 3).intValue();
        }

        JSONObject json = new JSONObject();

        try {
            json.put("node_count", nodeCount);
            json.put("total_size", totalSize);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return json;
    }
    public void generateAnalyzeDocument(JSONObject json) {

        Writer analyzeWriter = null;
        try {
            //Analyze file creates.
            analyzeWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileSavedPath + "analyze.json", true), "utf-8"));
            analyzeWriter.write(json.toString());
            analyzeWriter.flush();
            analyzeWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File is not created.");
        } finally {
            try {
                if (analyzeWriter != null)
                    analyzeWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //*********************Max memory**********************//

    //Test when user created charts - have some data
/*
    public void getMaxAfterUsedJSHeapSize() {
        getJSHeapSizeMax();
    }

    //getJSHeapSize method
    private void getJSHeapSizeMax() {
        Long memoryMaxHeapSize =(Long) ((JavascriptExecutor)driver).executeScript("return window.performance.memory.usedJSHeapSize");

        System.out.println(String.format("[%-12s]", LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME)) + ":" + Long.toString(memoryMaxHeapSize));
        memoryMaxList.put(memoryMaxHeapSize);
    }

    public void generateOutputMemoryMax(String fileMaxNameJson) {

        BufferedWriter jsonWriter = null;
        File file = null;
        try {
            //Values added to the JSON file
            jsonMaxSize.put("sizeOfMemoryLargeCharts", memoryMaxList);
            file = new File(fileSavedPath + fileMaxNameJson);
            //data writes into file
            jsonWriter = new BufferedWriter(new OutputStreamWriter(
                    //new FileOutputStream(fileSavedPath + fileNameJson, true), "utf-8"));
                    new FileOutputStream(file), "utf-8"));
            jsonWriter.write(jsonMaxSize.toString());
            jsonWriter.flush();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File is not created.");
        } finally {
            int timeListLength = memoryMaxList.length();
            for (int i = timeListLength - 1; i >= 0; i--) {
                memoryMaxList.remove(i);
            }
            try {
                if (jsonWriter != null)
                    jsonWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
*/
}

