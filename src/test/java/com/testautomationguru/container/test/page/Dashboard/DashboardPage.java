package test.java.com.testautomationguru.container.test.page.Dashboard;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import test.java.com.testautomationguru.container.test.page.PageObject;
import test.java.com.testautomationguru.container.test.utils.logging.CustomReporter;


/**
 * Created by user on 7/17/18.
 */
public class DashboardPage extends PageObject{

    private WebDriver driver;
    private static WebDriverWait wait;
    private Actions action;
    private SoftAssert softAssert;

    private DashboardPageDomElements domElements;


    public DashboardPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = (WebDriverWait) new WebDriverWait(driver, 30).withMessage("Element is not founded");
        this.action = new Actions(driver);
        this.softAssert = new SoftAssert();
        this.domElements = new DashboardPageDomElements(driver);
    }

    private void clearAndEnterData (WebElement element, String inputText) {
        DashboardPage.wait.until(ExpectedConditions.visibilityOf(element));
        element.clear();
        element.sendKeys(inputText);
        CustomReporter.log("Message: " + inputText + " is entered");
    }

    private void  clickOnWebelementFunctionality (WebElement element) {
        DashboardPage.wait.until(ExpectedConditions.visibilityOf(element));
        element.click();
        CustomReporter.log("Message: " + element + " is pressed on.");
    }

    private void dragAndDropAction (WebElement elementOne, WebElement elementTwo, String fieldFrom, String fieldTo) {
        DashboardPage.wait.until(ExpectedConditions.visibilityOf(elementTwo));
        action.dragAndDrop(elementOne, elementTwo).perform();
        CustomReporter.logAction("Element is added to the" + fieldFrom + "field -> " + fieldTo);
        //CustomReporter.logAction("Element is added to the 'Measures' field -> 'Wizard'");
    }
     /*
    *Method -  User logins into the Web Application.
    * */
    public void loginUser(String user, String pwd) {

        try {
            CustomReporter.log("**loginUser**");
            //Values are entered into the 'User' field
            clearAndEnterData(domElements.getInputUserField(), user);

            //Values are entered into the 'Password' field
            clearAndEnterData(domElements.getInputPasswordField(), pwd);

            //Confirm login
            clickOnWebelementFunctionality(domElements.getSubmitLoginButton());

            Thread.sleep(4000);

            CustomReporter.logAction("User successfully logins into the App");

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("User is not logged in into the App");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Threads no sleep.");
        }
    }

    public void createNewDashboardPage() {

        try {
            CustomReporter.log("**createNewDashboardPage**");
            this.driver.navigate().refresh();

            //Press on the "File" item at the Top menu.
            clickOnWebelementFunctionality(domElements.getFileTopMenu());

            //Press on "New" item into dropdown menu.
            clickOnWebelementFunctionality(domElements.getNewDropdownItem());

            //New schema selects for report
            clickOnWebelementFunctionality(domElements.getSelectSchemaItem());

            //Press on 'test2' schema for report
            clickOnWebelementFunctionality(domElements.getChooseBigSchema());

            //Confirm new schema creation
            clickOnWebelementFunctionality(domElements.getConfirmCreationSchema());

            //all process is completed
            CustomReporter.logAction("Schema is created.");
            Thread.sleep(2000);

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("New schema is not created.");
        } catch (NoSuchElementException ne) {
            ne.printStackTrace();
            System.out.println("No such element. Try later. Maybe problem with test2 schema");
        }catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Threads no sleep.");
        }
    }

    public void createNewCharts() {
        try {
            CustomReporter.log("**createNewCharts**");
            //Press on 'Diagrams' menu item.
            clickOnWebelementFunctionality(domElements.getDiagramsTopMenu());

            //Select the 'Serial' type of diagrams
            clickOnWebelementFunctionality(domElements.getSelectSerialType());

            //Select the 'More' function at the top-right part of diagram window
            clickOnWebelementFunctionality(domElements.getMoreChartFunction());

            //Press on the 'Edit' item on the diagram window
            clickOnWebelementFunctionality(domElements.getEditChartForm().get(0));

            CustomReporter.logAction("Edit Dashboard window is opened successful.");
            Thread.sleep(2000);

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("Edit window for diagrams is not created.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Threads no sleeps.");
        }
    }
    public void addSuggestionsToCharts() {

        try {
            CustomReporter.log("**addSuggestionsToCharts**");
            //Press on 'Measures' suggestions item.
            clickOnWebelementFunctionality(domElements.getChartsItems().get(0));

            //Move to 'Measures' the 'Eq Site Limit' parameter.
            dragAndDropAction (domElements.getMeasuresItems().get(3), domElements.getChartsParameters().get(0), "\'Measures\'", "\'Wizard\'");

            //Closes first item
            domElements.getChartsItems().get(0).click();

            //Press on 'County' suggestions item
            domElements.getChartsItems().get(2).click();

            //Move to 'Rows' the 'All-Level County' parameters
            dragAndDropAction (domElements.getCountyAllMembers().get(2), domElements.getChartsParameters().get(0), "\'Rows\'", "\'Wizard\'");
            //TODO dragAndDropAction method - some problem

            //Press on 'Line' suggestions item
            domElements.getChartsItems().get(3).click();

            //Move to 'Columns' the 'All-Level County' parameters
            dragAndDropAction (domElements.getCountyAllMembers().get(2), domElements.getChartsParameters().get(0), "\'Columns\'", "\'Wizard\'");

            //Press on 'Run' button at the 'Footer' part
            domElements.getFooterButton().get(2).click();

            //Press on 'Save' button at the 'Footer' part
            clickOnWebelementFunctionality(domElements.getFooterButton().get(0));
            Thread.sleep(2000);

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("New configuration is not added.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Thread can not sleeps.");
        }
    }
    public void loopOfSaving() {

        try {
            CustomReporter.log("**loopOfSaving**");
            //Open 'Edit' mode using the left menu icon.
            clickOnWebelementFunctionality(domElements.getEditLeftIcon());

            //save configuration after 'Edit' mode - no data changed
            clickOnWebelementFunctionality(domElements.getFooterButton().get(0));

            CustomReporter.logAction("Click on 'Save' button - 'Edit mode'");
            Thread.sleep(4000);

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("New configuration is not added.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Thread can not sleeps.");
        }
    }
    public void loopOfEdit() {
        try {
            CustomReporter.log("**loopOfEdit**");
            //Open 'Edit' mode using top menu.
            clickOnWebelementFunctionality(domElements.getMoreChartFunction());

            clickOnWebelementFunctionality(domElements.getEditLoopChartForm());

            //Configuration saves after 'Edit' mode - no changed use. Used element from cache
            clickOnWebelementFunctionality(domElements.getFooterButtonOnEditMode());
            Thread.sleep(4000);

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("New configuration is not added.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Thread can not sleeps.");
        }
    }

    public void closeDashboardItem(){

        try {
            CustomReporter.log("**closeDashboardItem**");
            //Press on 'File' top-menu item
            clickOnWebelementFunctionality(domElements.getFileTopMenu());

            //Select 'Close' item
            clickOnWebelementFunctionality(domElements.getFileCloseItem().get(13));
            CustomReporter.logAction("'Close' item selects");

            //Refresh page and waits GC.
            //this.driver.navigate().refresh();
            Thread.sleep(1200);

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("New configuration is not added.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Thread can not sleeps.");
        }
    }

    public void addSuggestionsToChartsWithLargeValues() {

        try {
            CustomReporter.log("**addSuggestionsToCharts**");
            //Press on 'Measures' suggestions item.
            clickOnWebelementFunctionality(domElements.getChartsItems().get(0));

            //Move to 'Measures' the 'Eq Site Limit' parameter.
            dragAndDropAction (domElements.getMeasuresItems().get(3), domElements.getChartsParameters().get(0), "\'Measures\'", "\'Wizard\'");
            //Closes first item
            domElements.getChartsItems().get(0).click();

            //Press on 'County' suggestions item
            domElements.getChartsItems().get(2).click();
            //Move to 'Rows' the 'All-Level County' parameters
            dragAndDropAction (domElements.getCountyAllMembers().get(2), domElements.getChartsParameters().get(0), "\'Rows\'", "\'Wizard\'");
            //Closes second item
            domElements.getChartsItems().get(2).click();

            //Press on 'Policy' suggestions item
            domElements.getChartsItems().get(5).click();
            //Move to 'Columns' the 'All-Level Policy Id' parameters
            dragAndDropAction (domElements.getCountyAllMembers().get(2), domElements.getChartsParameters().get(0), "\'Rows\'", "\'Wizard\'");

            //Press on 'Save' button at the 'Footer' part
            domElements.getFooterButton().get(0).click();
            CustomReporter.logAction("Click on 'Save' - Diagrams is added to Dashboards");
            Thread.sleep(60000);

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("New configuration is not added.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Thread can not sleeps.");
        }
    }
    public void memorySizePointOne(){
        try {
            //first memory point after the diagram is started
            CustomReporter.log("**firstPoint**");
            //Open 'Edit' mode using top menu.
            clickOnWebelementFunctionality(domElements.getMoreChartFunction());
            Thread.sleep(2000);

        } catch (UnsupportedOperationException ue) {
            ue.printStackTrace();
            System.out.println("New configuration is not added.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Thread can not sleeps.");
        }
    }

    public void memorySizePointTwo(){
        try {
            //second memory point after the diagram is started
            CustomReporter.log("**SecondPoint**");

            //Open 'Diagrams' top menu item.
            clickOnWebelementFunctionality(domElements.getFileTopMenu());
            CustomReporter.logAction("Click on 'Diagrams' item - top panel");
            Thread.sleep(2000);

        } catch (UnsupportedOperationException ux) {
            ux.printStackTrace();
            System.out.println("New configuration is not added.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Thread can not sleeps.");
        }
    }

    public void beforeTakeHeapSnapshot() {
        /*
        driver.navigate().refresh();

        dump = (HashMap<Object, Object>) ((JavascriptExecutor)driver).executeScript(":takeHeapSnapshot");

       //Create HeapSnapshotFile
        /* Writer writer = null;
        try {
            /*writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("fileWithHeepData.txt"), "utf-8"));
            System.out.println("Begin writing...");
            Writer finalWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("filenam1e.txt"), "utf-8"));
            dump.forEach((key, value) -> {
                try {
                    finalWriter.write(key + ":" + value);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            //for (Object key : dump.keySet()) {
            //    writer.write(key + ":" + this.dump.get(key) + "\n");
            //    writer.newLine();
            //    writer.flush();
            //    writer.close();
           // }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e){
            e.printStackTrace();
            System.err.println("File not found ...");
        }  catch (IOException e) {
            System.err.println("Problem writing to the file ...");
        }*/

        /*dump.forEach((key, value) -> System.out.println(key + ":" + value));*/
    }

}