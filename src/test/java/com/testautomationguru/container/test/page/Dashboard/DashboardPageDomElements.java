package test.java.com.testautomationguru.container.test.page.Dashboard;

import lombok.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import test.java.com.testautomationguru.container.test.page.PageObject;

import java.util.List;

/**
 * Created by user on 7/19/18.
 */

@Data @Getter @Setter
public class DashboardPageDomElements extends PageObject {

    /*Find elements on the page. */

    //Login page - User input field
    @CacheLookup
    @FindBy(css="input#userId")
    private WebElement inputUserField;

    //Login page - Pwd input field
    @CacheLookup
    @FindBy(css="input#userPwd")
    private WebElement inputPasswordField;

    //Login page - Submit button
    @CacheLookup
    @FindBy(css="input#loginButton")
    private WebElement submitLoginButton;

    //Dashboard page - Click on 'File' Top Menu
    @FindBy(css=".menu .btn-group .btn:first-child")
    private WebElement fileTopMenu;

    //Dashboard page - Click on 'New' dropdown-menu item
    @CacheLookup
    @FindBy(css=".dropdown-menu li:first-child")
    private WebElement newDropdownItem;

    //Dashboard page - Click on 'Schema' item
    @CacheLookup
    @FindBy(css="#schemaSelect")
    private WebElement selectSchemaItem;

    //Dashboard page - Choose big schema from dropdown-menu
    @CacheLookup
    @FindBy(css="option[value='test2']")
    private WebElement chooseBigSchema;

    //Dashboard page - Confirm creation of a new report
    @CacheLookup
    @FindBy(css="button.btn-primary:first-child")
    private WebElement confirmCreationSchema;

    //Diagrams page - Press on 'Diagrams' item
    @FindBy(css=".btn-group button:nth-child(3)")
    private WebElement diagramsTopMenu;

    //Diagrams page - Select the 'Serial' type of diagram
    @CacheLookup
    @FindBy(css=".btn-group .btn-group.open ul li:nth-child(2)")
    private WebElement selectSerialType;

    //Press on 'More' function
    @CacheLookup
    @FindBy(css=".ic3-controls .ic3-glyph-actions")
    private WebElement moreChartFunction;

    //Select the 'Edit' item
    @CacheLookup
    @FindBy(css=".ic3-widget-menu .item")
    private List<WebElement> editChartForm;

    //Select the 'Edit' item fro loop
    @FindBy(css=".ic3-widget-menu .item:nth-child(1)")
    private WebElement editLoopChartForm;

    //Find all items for charts suggestions
    @FindBy(css=".widget-options-panel .panel-default .hierarchy")
    private List<WebElement> chartsItems;

    //Open 'Measures' dropdown menu and select 'Eq Site Limit'
    @FindBy(css=".mdx-member-caption")
    private List<WebElement> measuresItems;

    //Select 'County' -> 'allMembers'
    @FindBy(css=".mdx-member-info")
    private List<WebElement> countyAllMembers;

    //Find all items for charts suggestions
    @FindBy(css=".panel .drop-placeholder")
    private List<WebElement> chartsParameters;

    //Find all footer button: 'Save', 'Cancel', 'Run'
    @FindBy(css=".widget-options-panel-footer .btn-default")
    private List<WebElement> footerButton;

    //Open 'Edit' mode at the left menu
    @FindBy(css="span.ic3-glyph-settings")
    private WebElement editLeftIcon;

    //Find all footer button: 'Save', 'Cancel', 'Run'
    @FindBy(css=".widget-options-panel-footer .btn-default:nth-child(1)")
    private WebElement footerButtonOnEditMode;

    //Select 'Close' item -> 'File' menu.
    @CacheLookup
    @FindBy(css=".dropdown-menu > li")
    private List<WebElement> fileCloseItem;

    public DashboardPageDomElements(WebDriver driver) {
        super(driver);
    }
}
